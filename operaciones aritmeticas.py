#SUMA
variable_1 = 8
variable_2 = 2
print(variable_1 + variable_2)

numero_de_pasteles = 0
print (numero_de_pasteles)
print (numero_de_pasteles + 6)

print (4+12)

#SUMA EN UNA LINEA

a1 = 3
a2 = 1
a3 = 5

suma = a1 + a2 + a3
print (suma)

#SUMA DE STRING
text_1 = "Hola"
text_2 = "Bienvenido"
print (text_1 + text_2)

#RESTA
numero_1 = 5
numero_2 = 4
print (numero_1 - numero_2)
print (numero_2 - numero_1)

#MULTIPLICACION
multi_1 = 3
multi_2 = 5
print (multi_1 * multi_2)

#DIVISION
print (40/8)

division_1 =76
division_2 = 4
print (division_1 / division_2)

#DIVISION ENTERA
print (4//2)

#POTENCIACION
print (2**3)

#MODULO
print (130 % 3)
